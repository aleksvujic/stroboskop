# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://aleksvujic@bitbucket.org/aleksvujic/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/aleksvujic/stroboskop/commits/7f87895c17949ce09a0cad60f91210deed88b5d6

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/aleksvujic/stroboskop/commits/1adc843a2734816fabb3b466dfb1b4bb85e6071a

Naloga 6.3.2:
https://bitbucket.org/aleksvujic/stroboskop/commits/bdbe09006f65dc88e3d9e0a6087d3672104cb5e3

Naloga 6.3.3:
https://bitbucket.org/aleksvujic/stroboskop/commits/2e8e08603488a20f3cfe100aa4e8fb99bbc0f79b

Naloga 6.3.4:
https://bitbucket.org/aleksvujic/stroboskop/commits/cdc8a6829c290fe7ead43dbb051d2e09bfe24724

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/aleksvujic/stroboskop/commits/da65100dab89dfe7349a0ac21dc43713fbbfff50

Naloga 6.4.2:
https://bitbucket.org/aleksvujic/stroboskop/commits/6c0f9dd123eb36d0317849e70d8caff3b93f73b3

Naloga 6.4.3:
https://bitbucket.org/aleksvujic/stroboskop/commits/25e820d05a6fd26b2b8ff211244f94878e9e1450

Naloga 6.4.4:
https://bitbucket.org/aleksvujic/stroboskop/commits/4ff6b0a6ef123401121647d00be57227794a195c